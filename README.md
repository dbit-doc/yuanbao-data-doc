# 元寶數據源系統接口文檔

# 文件目的
  - 本文檔介紹平台串接數據源接口參數規範、加密驗簽規則，並於文末提供各語言版本串接範例。


# 傳輸規範

  - 使用TCP/IP作為傳輸層協議
  - 使用HTTP作為應用層協議
  - 傳遞參數格式皆為json(須使用Content-type規範header)
  
  
# 安全規範

  - 所有接口必須校驗sign簽名值正確無誤,再處理業務邏輯


# 接口說明
## 元寶系統接口清單

  - bet-records/{language}-<a href="#bet-records/{language}(POST)">取得所有注單紀錄(POST)</a>
  - bet-records/{language}/{account}-<a href="#bet-records/{language}/{account}(POST)">取得單一會員注單紀錄(POST)</a>


## 參數說明(<a href="#parametersTable">參數說明</a>) 

### <p id="bet-records/{language}(POST)">bet-records/{language}-取得所有注單紀錄(POST)</p>
#### Reuqest

- Method: **POST**
- URL: ```/data-source/bet-records/{language}```
- Headers： Content-Type:application/json
- Body:
```
{
	"spId": "PL0001",
	"productId": "racing",
	"productTypeId": "1",
	"accountList": "GA001,GA002,GA003",
	"date": "20180501-20180504",
	"queryMode": "0",
	"querySystem": "0",
	"requestTime": "20180504163056",
	"sign": "c08d3e4cb641fbbdc5b06b448dd22395"
}
```

#### Response
- Body
```
{
	"retCode": "0",
	"data": {
		"count": 800,
		"sumAmt": 18000,
		"chatGameSystemEarn": -25000,
		"chatGameSystemCommission": 250,
		"betResult": [{
				"betNumber": "racing-20180103125640-1233",
				"gameName": "racing",
				"gameResult": "01,02,03,04,05,06,07,08,09,10",
				"gameNumber": "690066",				
				"content": {詳見注單內容對照表},
				"account": "GA205331acx",
				"status": "1",
				"createTime": "20180103125623",
				"updateTime": "20180103125746"
			},
			{
				"betNumber": "racing-20180103125640-1234",
				"gameName": "racing",
				"gameResult": "01,02,03,04,05,06,07,08,09,10",
				"gameNumber": "690066",				
				"content": {詳見注單內容對照表},
				"account": "GA205331acx",
				"status": "1",
				"createTime": "20180103125633",
				"updateTime": "20180103125746"
			},
			{
				"betNumber": "racing-20180103125640-1235",
				"gameName": "racing",
				"gameResult": "01,02,03,04,05,06,07,08,09,10",
				"gameNumber": "690066",
				"content": {詳見注單內容對照表},
				"account": "GA205331acx",
				"status": "1",
				"createTime": "20180103125633",
				"updateTime": "20180103125746"
			}
		]
	}
}
```


### <p id="bet-records/{language}/{account}(POST)">bet-records/{language}/{account}(POST)-取得單一會員注單紀錄(POST)</p>
#### Reuqest

- Method: **POST**
- URL: ```/data-source/bet-records/{language}/{account}```
- Headers： Content-Type:application/json
- Body:
```
{
	"spId": "PL0001",
	"productId": "racing",
	"productTypeId": "1",
	"date": "20180501-20180504",
	"queryMode": "0",
	"querySystem": "0",
	"requestTime": "20180504163056",
	"sign": "c08d3e4cb641fbbdc5b06b448dd22395"
}
```

#### Response
- Body
```
{
	"retCode": "0",
	"data": {
		"count": 800,
		"sumAmt": 18000,
		"chatGameSystemEarn": -25000,
		"chatGameSystemCommission": 250,
		"betResult": [{
				"betNumber": "racing-20180103125640-1234",
				"gameName": "racing",
				"gameResult": "01,02,03,04,05,06,07,08,09,10",
				"gameNumber": "690066",
				"content": {詳見注單內容對照表},
				"account": "GA205331acx",
				"status": "1",
				"createTime": "20180103125623",
				"updateTime": "20180103125746"
			},
			{
				"betNumber": "racing-20180103125640-1234",
				"gameName": "racing",
				"gameResult": "01,02,03,04,05,06,07,08,09,10",
				"gameNumber": "690066",
				"content": {詳見注單內容對照表},
				"account": "GA205331acx",
				"status": "1",
				"createTime": "20180103125623",
				"updateTime": "20180103125746"
			},
			{
				"betNumber": "racing-20180103125640-1234",
				"gameName": "racing",
				"gameResult": "01,02,03,04,05,06,07,08,09,10",
				"gameNumber": "690066",
				"content": {詳見注單內容對照表},
				"account": "GA205331acx",
				"status": "1",
				"createTime": "20180103125623",
				"updateTime": "20180103125746"
			}
		]
	}
}
```


## <p id="parametersTable">參數說明</p>
|參數名稱|參數名稱|參數格式|參數說明|可否選填|
|:--|:--|:--|:--|:--|
|productId||Integer(14)|依照產品對照表|與productTypeId擇一填|
|productTypeId||Integer(14)|依照產品類別對照表|與productId擇一填|
|language||String(16)|返回注單內容語言(有以下枚舉值:en、zh-cn、zh-tw)|必填|
|spId||String(32)|平台ID(由元寶系統發配)|必填|
|accountList||StringArray|玩家帳號列表(調用getAll接口可選填)|非必|
|account||String(32)|玩家帳號|非必(如已有 請勿填accountList)|
|type||String(16)|遊戲種類(詳見<a href="#productTable">產品對應表</a>,或傳入all查詢所有)|必填|
|date||String(17)|查詢起迄,可接受兩種格式,ex:yyyymmdd或yyyymmddHHmmss,中間以-為分隔符號(缺省值為當日)|非必|
|queryMode||String(1)|查詢模式,0:以創建時間為搜尋條件,1:以更新時間為搜尋條件(缺省值為0)|非必|
|querySystem||String(1)|是否查詢系統盈虧抽水,0:不搜尋,1:搜尋(缺省值為0)|非必|
|requestTime||String(14)|接口請求時間(yyyyMMddHHmmss)|必填|
|sign||String(32)|簽名(詳見<a href="#signDirection">簽名機制說明)</a>|必填|
|count||Integer(11)|總注單數|必填|
|sumAmt||Integer(11)|總碼量(單位:分)|必填|
|chatGameSystemEarn||Integer(11)|系統總輸贏,僅對戰類有值(單位:分)|必填|
|chatGameSystemCommission||Integer(11)|系統總輸贏抽水,僅對戰類有值(單位:分)|必填|
|betResult||JsonArray||必填|
||betNumber|String(32)|注單編號|必填|
||gameName|String(32)|遊戲名稱|必填|
||gameResult|String(32)|遊戲結果|必填|
||gameNumber|String(32)|遊戲局號|必填|
||content|JsonObject|投注內容(詳見<a href="#betResultTable">注單內容對照表</a>)|必填|
||status|String(1)|注單狀態(0:待派彩,1:已派彩,d:未開牌(對戰類))|必填|
||createTime|String(14)|創建時間(yyyyMMddHHmmss)(為0時表示待處理)|必填|
||updateTime|String(14)|更新時間(yyyyMMddHHmmss)(為0時表示待處理)|必填|
  
  
## <p id="productTable">產品對照表</p>
|產品ID(productId)|遊戲名稱|
|:--|:--|
|racing|彩票-北京賽車|
|rowing|彩票-幸運飛艇|
|timetime|彩票-重慶時時彩|
|self-racing|彩票-疾速賽車|
|gd11x5|彩票-廣東11選五|
|js11x5|彩票-江蘇11選五|
|jx11x5|彩票-江西11選五|
|ffc11x5|彩票-分分彩11選五|
|bjk3|彩票-北京快三|
|gsk3|彩票-甘肅快三|
|gxk3|彩票-廣西快三|
|hebk3|彩票-河北快三|
|hubk3|彩票-湖北快三|
|jsk3|彩票-江蘇快三|
|ffck3|彩票-分分彩快三|
|dragon-tiger|電子-龍虎|
|dice-bao|電子-骰寶|
|cowcow|對戰-牛牛|
|this-bar|對戰-二八槓|
|red-box|對戰-歡樂紅包|
|three-cards|對戰-三公|

## <p id="productTypeIdTable">產品類別對照表</p>
|產品類別ID(productTypeId)|類別名稱|遊戲名稱|
|:--|:--|:--|
|1|彩票類|北京賽車,重慶時時彩,幸運飛艇,11選5,快3,疾速賽車|
|2|對戰類|牛牛,二八槓,歡樂紅包,三公|
|3|微信類||
|4|棋牌類|龍虎,骰寶|


## <p id="betResultTable">注單內容對照表</p>
|產品ID(productId)|參數名稱|參數格式|參數說明|
|:--|:--|:--|:--|
|racing、rowing、timetime、11x5類、k3類、疾速賽車||||
||type|String(16)|下注種類|
||content|String(16)|下注內容|
||amt|Integer(11)|下注金額(單位:分)|
||earn|Integer(11)|輸贏(正值為玩家贏、負值為玩家輸)(單位:分)||
|cowcow、this-bar、red-box、three-caards||||
||role|String(8)|閒家/莊家|
||cards|String(32)|取得牌組|
||point|String(16)|最佳牌型|
||amt|Integer(11)|下注金額/收注總額(單位:分)|
||commission|Integer(11)|抽水(單位:分)|
||earn|Integer(11)|輸贏(正值為玩家贏、負值為玩家輸、若與amt相同則為和)(單位:分)||
|dragon-tiger、dice-bao||||
||type|String(16)|下注種類|
||content|String(16)|下注內容|
||amt|Integer(11)|下注金額(單位:分)|
||earn|Integer(11)|輸贏(正值為玩家贏、負值為玩家輸)(單位:分)||








## <p id="signDirection">簽名機制說明</p>


# 待更新項目

 - 須加上簽名演算範例
 - 須補全局錯誤碼
 

# 更新日誌

 - 2018/05/04	建立文檔	by davidccc
 - 2018/05/17	產品ID修正	by davidccc
 - 2018/07/03	增加注單狀態、回應時間	by davidccc
 - 2018/07/03	時間查詢條件增加接受格式	by davidccc
 - 2018/07/03	新增queryMode、gameNumber參數	by davidccc
 - 2018/08/04	新增電子龍虎、電子骰寶注單類別	by davidccc 
 - 2018/08/08	新增參數accountList	by iwillGitHub
   
# 附錄：

 - <a href="https://github.com/TorchStoneRD/yuanbao-demo">文檔與DEMO包</a>
